#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2017 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import re
import requests
import shutil
import tempfile
from sqlalchemy.orm import mapper
from sh import pdftotext

from config import ConfigPlantilla
from data_converter.data_converter import DataConverter
from plantilles.plantilla import Plantilla
from OpenSSL.crypto import verify

class PlantillesConverter(DataConverter):
    def __init__(self, config_file_path):
        self.config = ConfigPlantilla(config_file_path)
        super(PlantillesConverter, self).__init__()
        mapper(Plantilla, self.table) 
        logging.basicConfig(filename=self.config.log_file, 
                            level=logging.WARNING)

    def get_input_files(self):
        """Get DOGC in pdf format"""
        urls = open(self.config.urls).readlines()
        remote_file = urls[0]
        local_file = os.path.join(self.config.raw_data_dir, 'plantilles.pdf')
        response = requests.get(remote_file, verify=False)
        with open(local_file, 'wb') as f:
            f.write(response.content)
 
    def convert(self):
        pdftotext('-nopgbrk', 
                  os.path.join(self.config.raw_data_dir, 'plantilles.pdf'), 
                  os.path.join(self.config.data_dir, "plantilles.txt"))
 
    def get_valid_records(self):
        # Get records
        records = []
        # Remove lines without value
        self.remove_lines()
        # Loop cleaned file and create records
        filename = os.path.join(self.config.data_dir, "plantilles.txt")
        eoi = False
        pri = False
        sec = False
        with open(filename, "r") as r:
            for l in r:
                l = l.strip()
                # Check when an annex is started. Example: "Annex 3"
                match_annex = re.search("^Annex ([0-9])", l)
                # Check if line is a centre. Example: 08052670: Escola Auró
                match_centre = re.search("^([0-9]{8}): (.+)", l)
                if match_annex:                    
                    annex = int(match_annex.group(1))
                elif match_centre:
                    eoi = False
                    pri = False
                    sec = False
                    centre_code = match_centre.group(1)

                    # Remove type of centre of the name
                    centre_name = match_centre.group(2).replace("(A)","") \
                                                       .replace("(CMC)", "") \
                                                       .replace("(EDIF)", "") \
                                                       .replace("(CFACP)", "")                    
                    if "EOI" in centre_name:
                        eoi = True
                elif l == "Educació infantil i primària:" or \
                     l == "Educació infantil i primària" or \
                     l == "Educació primària:" or \
                     l == "Educació primària":
                    # If this line is read, especialities are of primary education
                    sec = False
                    pri = True
                elif l == "Educació secundària obligatòria:" or \
                     l == "Educació secundària obligatòria" or \
                     l == "Educació secundària obligatòria i batxillerat:" or \
                     l == "Educació secundària obligatòria i batxillerat" or \
                     l == "Educació secundària obligatòria i cicles formatius:" or \
                     l == "Educació secundària obligatòria i cicles formatius" or \
                     l == "Dansa i Música:" or \
                     l == "Dansa i Música":
                    # If this line is read, especialities are of secondary education
                    # They may be FP especiality, but we cannot differientate them
                    pri = False
                    sec = True
                else:
                    # If line is not a header, it is a especiality 
                    # (with perfil or not) and plantilla
                    # Example with perfil: Educació infantil (DIV): 2
                    # Example without perfil: Música: 1
                    esp_perf_plan = l.split(":")
                    ### NO FUNCIONA BÉ
                    ### En els casos de Llengua estrabgera: anglès: 4
                    esp_perf = ":".join(esp_perf_plan[0:-1]) 
                    plan = esp_perf_plan[-1]
                    # Remove the kind of place, not needed
                    esp_perf = esp_perf.replace("(E)", "") \
                                       .replace("(L)", "") \
                                       .replace("(590EC)", "") \
                                       .replace("(591EC)", "") \
                                       .replace("ICAS", "")
                    # Split perfil if it exists                                  
                    match_perf = re.search("(.+)\(([A-Z]{1,3})\)", esp_perf)
                    if match_perf:
                        esp = match_perf.group(1)
                        perf = match_perf.group(2)
                    else:
                        esp = esp_perf
                        perf = ""
                    cos = PlantillesConverter.setcos(annex, eoi, pri, sec)

                    #ugly hack for abreviation, bad names, ...
                    # there codo to substitute some keywords with errors

                    #build object and appen
                    record = [annex, centre_code, esp, perf, plan, cos]
                    records.append(record)
        return records
    
    @staticmethod
    def setcos(annex, eoi, pri, sec):
        cos  = ''        
        if annex == 1:
            cos = 597
        elif annex == 2:
            # They may be FP especiality, but we cannot differientate them
            cos = 590
        elif annex == 6:
            # They may be FP especiality, but we cannot differientate them
            cos = 590
        if eoi:
            cos = 592
        elif pri:
            cos = 597
        elif sec:
            cos = 590
        return cos            
        
    def remove_lines(self):
        filename = os.path.join(self.config.data_dir, "plantilles.txt")
        contains_list=["Plantilles de",
                       "Plantilla de",
                       "ensenyaments de l'àmbit d'arts plàstiques i disseny.",
                       "(L) Personal laboral",
                       "(L) Places ocupades per personal laboral",
                       "Centre amb exigència del requisit d'aranès",
                       "(RF) Reforç de foment de",
                       "de màxima complexitat",
                       "Centre de dificultat especial",
                       "Centre de formació d'adults en centre penitenciari",
                       "Centre de formació d’adults en centre penitenciari",
                       "Llocs de treball i dotacions",
                       "ISSN",
                       "www.gencat.cat/dogc",
                       "DL B ",
                       "Diari Oficial de la Generalitat de Catalunya",
                       "Núm. ",
                       "CVE-DOGC-",
                       "Municipi:",
                       "Consorci d'Educació de Barcelona",
                       "Serveis Territorials",
                       "Serveis territorials",
                       "116 /1209"]
        self.clean_source(os.path.join(self.config.data_dir, filename), contains_list)
        # Remove lines until first Annex and remove page numbers
        with open(filename, "r") as r, \
             tempfile.NamedTemporaryFile(mode='w', delete=False) as w:
            # Read until first Annex without writing these lines
            line = r.readline()
            while not re.search("^Annex", line):
                line = r.readline()
            # Read the rest of lines and write them except if they are a 
            # page number
            while line:
                if not re.search("[0-9]+\/[0-9]+", line) and \
                   not re.search("[0-9]+ \/[0-9]+", line) and \
                       not re.search("\([0-9]+\.[0-9]+\.[0-9]+\)", line):
                    w.write(line)
                line = r.readline()
            w.close()
            # Replace plantilles file by cleaned one
            shutil.move(w.name, filename)
 
    def build_objects(self, raw_records):
        """
        Build a list of objects from raw_records getting interesting fields.
        Needed fields are:
            - codi_centre: centre code
            - nom_centre = centre name
            - codi_especialitat: especialitat code
            - nom_especialitat: especialitat name
            - perfil: perfil or kind of place
            - places    
        """
        # Set with especialitats without code
        esp_without_code = set()
        # Each record has the following data: 
        #   [annex, centre_code, esp, perf, plan, cos]

        # Loop all records
        for r in raw_records:
            plantilla = Plantilla()
            ### codi_centre
            plantilla.codi_centre = r[1].strip()
            ### codi_especialitat
            plantilla.codi_especialitat = ''
            # If cos is set, find especialitat code of this cos
            if r[5] != '':
                plantilla.codi_especialitat = self.guess_especialitat_code(r[2].strip(), r[5])
            # If cos is not set or especialitat is not found, find especialitat
            # without taking account cos
            if plantilla.codi_especialitat == '???' or plantilla.codi_especialitat == '':
                plantilla.codi_especialitat = self.guess_especialitat_code(r[2].strip())            
            ### nom_especialitat
            plantilla.nom_especialitat = r[2].strip()
            # If especialitat code is not found, add the especialitat name to a set
            # This set will be printed when loop is finished
            # This object will not be saved to database because of integrity error
            if plantilla.codi_especialitat == '???':
                esp_without_code.update((plantilla.nom_especialitat,))
            ### perfil
            plantilla.perfil = r[3].strip()
            ### plan
            plantilla.places = r[4].replace(",", ".").strip()
            # Add final nomenament to the collection of nomenaments
            self.objects.append(plantilla)
        # Print all especialitats without code (this set should be empty)
        # If any especialitat is printed, add code to database, delete plantilles table 
        # and rerun thi script
        print("Especialitats sense codi:")
        for e in esp_without_code:
            print(e)