#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2017 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os

from plantilles.plantilles_converter import PlantillesConverter

parser = argparse.ArgumentParser()
# -l: do not download data. use local data already present
# -e: do not download data nor convert existing raw_data. Use existing CSV files in data folder.
parser.add_argument("-l", "--localdata", action='store_true')
parser.add_argument("-e", "--onlyextract", action='store_true')
args = parser.parse_args()

nc = PlantillesConverter(os.path.join(
             os.path.dirname(os.path.realpath(__file__)),
             'config'))
if not args.onlyextract:
    if not args.localdata:
        print("Obtenint dades")
        nc.get_data()
    print("Convertint dades a un format llegible")
    nc.convert()
print("Extraient dades")
nc.parse()
print("Exportant a la base de dades")
nc.export()
print("Fi")