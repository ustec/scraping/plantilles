#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2017 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from data_converter.output_object import OutputObject


class Plantilla(OutputObject):
    def __init__(self):
        self.codi_centre = ""
        self.codi_especialitat = ""
        self.nom_especialitat = ""
        self.perfil = ""
        self.places = 0       

    def __str__(self):
        return "[codi_centre: " + str(self.codi_centre) + \
               ", codi_especialitat: " + self.codi_especialitat + \
               ", nom_especialitat: " + self.nom_especialitat + \
               ", perfil: " + self.perfil + \
               ", places: " + str(self.places) + \
               "]"
