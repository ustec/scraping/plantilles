CREATE TABLE IF NOT EXISTS `plantilla1819` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codi_centre` varchar(8) DEFAULT NULL,
  `codi_especialitat` varchar(5) DEFAULT NULL,
  `nom_especialitat` varchar(255) DEFAULT NULL,
  `perfil` varchar(4) DEFAULT NULL,
  `places` decimal(11,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`codi_centre`) REFERENCES `centre` (`codi_centre`),
  FOREIGN KEY (`codi_especialitat`) REFERENCES `especialitat` (`codi`)
)