import configparser
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func

# Read config data
config = configparser.ConfigParser()
config.read("config")
host = config.get('database', 'HOST')
user = config.get('database', 'USER')
password = config.get('database', 'PASSWORD')
database = config.get('database', 'DATABASE_NAME')
table_name_plant1718 = config.get('database', 'TABLE_PLANT1718')
table_name_plant1617 = config.get('database', 'TABLE_PLANT1617')
table_name_centre = config.get('database', 'TABLE_CENTRE')
table_name_st = config.get('database', 'TABLE_ST')

# Prepare sqlalchemy session
engine_string = "mysql://" + user + ":" + password + "@" + host + "/" + database
eng = create_engine(engine_string)
metadata = MetaData(eng)
table_plant1718 = Table(table_name_plant1718, metadata, autoload=True)
table_plant1617 = Table(table_name_plant1617, metadata, autoload=True)
table_centre = Table(table_name_centre, metadata, autoload=True)
table_st = Table(table_name_st, metadata, autoload=True)
Session = sessionmaker(bind=eng)
session = Session()

# Load all serveis territorials
serveis_territorials = session.query(table_st.c.codi, table_st.c.nom)
# For each servei territorial, create a csv file

# TODO: IT IS NOT WORKING!!!! Look at 1.csv

for st in serveis_territorials.all():
    f = open("data/" + str(st[0]) + ".csv","w")
    # Write headers
    f.write("Codi;;17/18;16/17;Dif\n")
    # Get all centres of ech servei territorial
    centres_st = session.query(table_centre.c.codi_centre, table_centre.c.nom_centre) \
                        .join(table_st) \
                        .filter(table_st.c.codi == st[0])
    total_centres_1718 = 0
    total_centres_1617 = 0
    total_dif_centre = 0
    # For each centre get its places
    for c in centres_st.all():
        # For each centre get total places for each year
        plantilla_centre_1718 = session.query(func.sum(table_plant1718.c.places)) \
                                       .filter(table_plant1718.c.codi_centre==c[0])
        plantilla_centre_1617 = session.query(func.sum(table_plant1617.c.places)) \
                                       .filter(table_plant1617.c.codi_centre==c[0])
        places_centre_1718 = plantilla_centre_1718[0][0] if plantilla_centre_1718[0][0] is not None else 0
        places_centre_1617 = plantilla_centre_1617[0][0] if plantilla_centre_1617[0][0] is not None else 0
        dif_centre = places_centre_1718 - places_centre_1617
        # Write total places of each centre
        f.write(c[0] + ";" + c[1] + ";" + \
                str(places_centre_1718).replace(".", ",") + ";" + \
                str(places_centre_1617).replace(".", ",") + ";" + \
                str(dif_centre).replace(".", ",") + \
                "\n")
        # Accumulate total of centres
        total_centres_1718 = total_centres_1718 + places_centre_1718
        total_centres_1617 = total_centres_1617 + places_centre_1617
        # For each centre, get its especialitats over las two years
        esps_centre_1718 = session.query(table_plant1718.c.codi_especialitat, table_plant1718.c.perfil) \
                                  .filter(table_plant1718.c.codi_centre==c[0])
        esps_centre_1617 = session.query(table_plant1617.c.codi_especialitat, table_plant1617.c.perfil) \
                                  .filter(table_plant1617.c.codi_centre==c[0])
        esps_centre = esps_centre_1718.union(esps_centre_1617) \
                                      .order_by(table_plant1617.c.codi_especialitat, 
                                                table_plant1617.c.perfil)
        # For each especialitat of the centre, get how many places for each year
        for e in esps_centre.all():
            plant_esp_1718 = session.query(table_plant1718.c.places) \
                                    .filter(table_plant1718.c.codi_centre==c[0]) \
                                    .filter(table_plant1718.c.codi_especialitat==e[0]) \
                                    .filter(table_plant1718.c.perfil==e[1])
            if plant_esp_1718.first() is None:
                p1718 = 0;
            else:
                p1718 = plant_esp_1718.first()[0]
            plant_esp_1617 = session.query(table_plant1617.c.places) \
                                    .filter(table_plant1617.c.codi_centre==c[0]) \
                                    .filter(table_plant1617.c.codi_especialitat==e[0]) \
                                    .filter(table_plant1617.c.perfil==e[1])
            if plant_esp_1617.first() is None:
                p1617 = 0;
            else:
                p1617 = plant_esp_1617.first()[0]
            dif = p1718 - p1617                         
            f.write(e[0] + ";" + e[1] + ";" + \
                    str(p1718).replace(".", ",") + ";" + \
                    str(p1617).replace(".", ",") + ";" +\
                    str(dif).replace(".", ",") + \
                    "\n")
    total_dif = total_centres_1718 - total_centres_1617
    f.write(";TOTALS;" + \
            str(total_centres_1718).replace(".", ",") + ";" + \
            str(total_centres_1617).replace(".", ",") + ";" + 
            str(total_dif).replace(".", ","))
    f.close()